# Finding unique letters triplet

Very simple python script that extracts unique letter triplets (such as "aba", "aca", etc.) from a dictionnary (in french).

Tested with two files : a French dictionnary (360 000+ words) and a list of first names (14 000 items).
#!/usr/bin/python
# -*- coding: utf-8 -*-

from unidecode import unidecode

import sys
import re
import string
import time


def crawlWord (word, combSet):
    "Crawls word for every new unique combination of 3 letters"
    
    wordLength = len(word)
    i = 0

    while i <= wordLength - 3:
        # Some words may contain hyphens
        if word[i+2] == '-':
            # Skip word if less than 2 characters after hyphen
            # (ex: "abc-de")
            if wordLength - i - 3 < 3: 
                break
            # Otherwise, go to next triplet
            else:
                i += 3
                next
        else:
            triplet = word[i:i+3]
            i += 1
            combSet.add(triplet)

    return

def crawlFile (fileName):
    "Each line is read, formatted and then fed to crawlWord"

    allTriplets = set()
    alphanum = re.compile('[\W_]+')

    startTime = time.time()

    with open(fileName, 'r')as infile:
        for line in infile:
            line = unidecode(line)
            line = line.lower()
            line = alphanum.sub('', line)

            if len(line) >= 3:
                crawlWord(line, allTriplets)

    endTime = time.time()

    print('Extracted {nb} triplets from {fileName} in {time}s'.format(
        nb = len(allTriplets),
        fileName = fileName,
        time = endTime-startTime))
    
    return allTriplets

def exportTriplets (fileName, triplets):
    "Export triplets to a file"

    with open(fileName, 'w') as file:
        for item in triplets:
            file.write(item + '\n')

dictFilename = 'dictionnary.fr.txt'
dictLogFilename = 'unique-letters-triplet-dict.fr.txt'
fNameFilename = 'firstNames.fr.txt'
fNameLogFilename = 'unique-letters-triplet-fName.fr.txt'

# Extract triplets from a dictionnary
dictTriplets = crawlFile(dictFilename)
exportTriplets(dictLogFilename, dictTriplets)

# Extract triplets from a list of first names
fNameTriplets = crawlFile(fNameFilename)
exportTriplets(fNameLogFilename, fNameTriplets)
